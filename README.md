# P�titions

![petitions](./prive/themes/spip/images/petition-xx.svg)


Comment mettre en ligne une p�tition, �ventuellement multilingue, avec un squelette SPIP et ses crit�res sp�cifiques, et suivre la collecte des signatures gr�ce � son flux RSS, son moteur de recherche appliqu� aux signatures, ses statiques en diff�rents formats servant aussi de navigation etc.


**Documentation officielle**\
https://contrib.spip.net/5469
