<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// si signature de petition, l'enregistrer avant d'afficher la page
// afin que celle-ci contienne la signature
if (isset($_GET['var_confirm'])) {
	$confirmer_signature = charger_fonction('confirmer_signature', 'action');
	$confirmer_signature($_GET['var_confirm']);
}
