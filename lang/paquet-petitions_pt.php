<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=pt
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petições em SPIP',
	'petitions_slogan' => 'Gestão das petições em SPIP',
];
