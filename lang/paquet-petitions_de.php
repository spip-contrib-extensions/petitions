<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petitionen mit SPIP',
	'petitions_slogan' => 'Verwaltung von Petitionen mit SPIP',
];
