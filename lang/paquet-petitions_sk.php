<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petície v SPIPe',
	'petitions_slogan' => 'Správa petícií v SPIPe',
];
