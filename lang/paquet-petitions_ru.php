<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Модуль сбора подписей для SPIP',
	'petitions_slogan' => 'Модуль сбора подписей для SPIP',
];
