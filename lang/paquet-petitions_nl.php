<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petities in SPIP',
	'petitions_slogan' => 'Beheer van petities in SPIP',
];
