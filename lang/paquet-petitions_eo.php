<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petskriboj en SPIP',
	'petitions_slogan' => 'Mastrumado de la petskriboj en SPIP',
];
