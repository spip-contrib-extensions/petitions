<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=oc_ni_mis
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Peticioun en SPIP',
	'petitions_slogan' => 'Gestioun dei peticioun en SPIP',
];
