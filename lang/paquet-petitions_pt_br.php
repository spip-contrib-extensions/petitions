<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petições do SPIP',
	'petitions_slogan' => 'Gerenciamento de petições do SPIP',
];
