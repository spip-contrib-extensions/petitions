<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=fr_tu
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Pétitions dans SPIP',
	'petitions_slogan' => 'Gestion des pétitions dans SPIP',
];
