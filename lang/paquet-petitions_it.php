<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Petizioni di SPIP',
	'petitions_slogan' => 'Gestione delle petizioni di SPIP',
];
