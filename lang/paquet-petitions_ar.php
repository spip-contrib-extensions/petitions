<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'العرائض في SPIP',
	'petitions_slogan' => 'إدارة العرائض في SPIP',
];
