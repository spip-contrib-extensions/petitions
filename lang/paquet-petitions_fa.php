<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'طومارها در اسپيپ',
	'petitions_slogan' => 'مديريت طومارها در اسپيپ',
];
