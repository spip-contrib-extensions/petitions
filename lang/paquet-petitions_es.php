<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'Peticiones en SPIP',
	'petitions_slogan' => 'Administración de las peticiones en SPIP',
];
