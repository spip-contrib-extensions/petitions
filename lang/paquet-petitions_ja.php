<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-petitions?lang_cible=ja
// ** ne pas modifier le fichier **

return [

	// P
	'petitions_description' => 'SPIPにおける請願',
	'petitions_slogan' => 'SPIPにおける請願の管理',
];
