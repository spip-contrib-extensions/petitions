<?php

/***************************************************************************\
 *  SPIP, Système de publication pour l'internet                           *
 *                                                                         *
 *  Copyright © avec tendresse depuis 2001                                 *
 *  Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribué sous licence GNU/GPL.     *
\***************************************************************************/

/**
 * Gestion de l'action editer_petition
 *
 * @package SPIP\Petitions\Actions
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_editer_petition_dist($arg = null) {

	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// si id_petition n'est pas un nombre, c'est une creation
	// mais on verifie qu'on a toutes les donnees qu'il faut.
	if (!$id_petition = intval($arg)) {
		$id_article = _request('id_article');
		if (!($id_article)) {
			include_spip('inc/headers');
			redirige_url_ecrire();
		}
		$id_petition = petition_inserer($id_article);
	}

	// Enregistre l'envoi dans la BD
	if ($id_petition > 0) {
		$err = petition_modifier($id_petition);
	}

	return [$id_petition, $err];
}

/**
 * Mettre à jour une petition existante
 *
 * @param int $id_petition
 * @param array $set
 * @return string
 */
function petition_modifier($id_petition, $set = null) {
	$err = '';

	include_spip('inc/modifier');
	include_spip('inc/filtres');
	$c = collecter_requests(
		// white list
		objet_info('petition', 'champs_editables'),
		// black list
		['statut', 'id_article'],
		// donnees eventuellement fournies
		$set
	);

	if (
		$err = objet_modifier_champs(
			'petition',
			$id_petition,
			[
			'data' => $set,
			],
			$c
		)
	) {
		return $err;
	}

	// changement d'article ou de statut ?
	$c = collecter_requests(['statut', 'id_article'], [], $set);
	$err .= petition_instituer($id_petition, $c);

	return $err;
}

/**
 * Insérer une petition en base
 *
 * @param int $id_article
 *     Identifiant de l'article recevant la pétition
 * @param array|null $set
 * @return int
 *     Identifiant de la pétition
 */
function petition_inserer($id_article, $set = null) {

	// Si id_article vaut 0 ou n'est pas definie, echouer
	if (!$id_article = intval($id_article)) {
		return 0;
	}

	$champs = [
		'id_article' => $id_article,
	];

	if ($set) {
		$champs = array_merge($champs, $set);
	}

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_insertion',
		[
			'args' => [
				'table' => 'spip_petitions',
			],
			'data' => $champs
		]
	);

	$id_petition = sql_insertq('spip_petitions', $champs);

	pipeline(
		'post_insertion',
		[
			'args' => [
				'table' => 'spip_petitions',
				'id_objet' => $id_petition
			],
			'data' => $champs
		]
	);

	return $id_petition;
}


/**
 * Institution d'une pétition
 *
 * @param int $id_petition
 *     Identifiant de la pétition
 * @param array $c
 *     Liste des champs à modifier
 * @return string|null
 */
function petition_instituer($id_petition, $c) {

	include_spip('inc/autoriser');
	include_spip('inc/modifier');

	$row = sql_fetsel('id_article,statut', 'spip_petitions', 'id_petition=' . intval($id_petition));
	$statut_ancien = $statut = $row['statut'];
	#$date_ancienne = $date = $row['date_time'];
	$champs = [];

	$s = $c['statut'] ?? $statut;

	// cf autorisations dans inc/petition_instituer
	if ($s != $statut /*OR ($d AND $d != $date)*/) {
		$statut = $champs['statut'] = $s;

		// En cas de publication, fixer la date a "maintenant"
		// sauf si $c commande autre chose
		// ou si l'petition est deja date dans le futur
		// En cas de proposition d'un petition (mais pas depublication), idem
		/*
		if ($champs['statut'] == 'publie') {
			if ($d)
				$champs['date_time'] = $date = $d;
			else
				$champs['date_time'] = $date = date('Y-m-d H:i:s');
		}*/
	}

	// Envoyer aux plugins
	$champs = pipeline(
		'pre_edition',
		[
			'args' => [
				'table' => 'spip_petitions',
				'id_objet' => $id_petition,
				'action' => 'instituer',
				'statut_ancien' => $statut_ancien,
			],
			'data' => $champs
		]
	);

	if (!(is_countable($champs) ? count($champs) : 0)) {
		return;
	}

	// Envoyer les modifs.
	sql_updateq('spip_petitions', $champs, 'id_petition=' . intval($id_petition));

	// Invalider les caches
	include_spip('inc/invalideur');
	suivre_invalideur("id='petition/$id_petition'");
	suivre_invalideur("id='article/" . $row['id_article'] . "'");

	// Pipeline
	pipeline(
		'post_edition',
		[
			'args' => [
				'table' => 'spip_petitions',
				'id_objet' => $id_petition,
				'action' => 'instituer',
				'statut_ancien' => $statut_ancien,
			],
			'data' => $champs
		]
	);

	// Notifications
	if ($notifications = charger_fonction('notifications', 'inc')) {
		$notifications(
			'instituerpetition',
			$id_petition,
			['statut' => $statut, 'statut_ancien' => $statut_ancien]
		);
	}

	return ''; // pas d'erreur
}

function revision_petition($id_petition, $c = null) {
	return petition_modifier($id_petition, $c);
}
